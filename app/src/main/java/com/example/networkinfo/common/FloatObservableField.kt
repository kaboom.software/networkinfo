package com.example.networkinfo.common

class FloatObservableCalcField
{

    val current: StringObservableField = StringObservableField()
    val maximum: StringObservableField = StringObservableField()
    val minimum: StringObservableField = StringObservableField()
    val average: StringObservableField = StringObservableField()

    private var mCurrent     : Float = 0f
    private var mMaxValue    : Float = 0f
    private var mMinValue    : Float = 0f
    private var mAccumulator : Float = 0f
    private var mCounter     : Int   = 0
    var mFormatString: String = ""

    fun reset()
    {
        mCurrent     = 0f
        mMaxValue    = 0f
        mMinValue    = 0f
        mAccumulator = 0f
        mCounter     = 0

        current.set("")
        maximum.set("")
        minimum.set("")
        average.set("")
    }

    fun set(value: Float)
    {
        mCurrent = value
        mCounter++
        mAccumulator += value
        if (value > mMaxValue) mMaxValue = value
        if (value < mMinValue) mMinValue = value

        current.set(mFormatString.format(mCurrent))
        maximum.set(mFormatString.format(mMaxValue))
        minimum.set(mFormatString.format(mMinValue))
        average.set(mFormatString.format(mAccumulator / mCounter))
    }

    fun get(): Float {return mCurrent}

}