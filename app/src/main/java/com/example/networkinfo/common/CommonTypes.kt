package com.example.networkinfo.common

//import android.databinding.ObservableField
import androidx.databinding.ObservableField
import java.math.BigDecimal

class StringObservableField  : ObservableField<String>()
class IntegerObservableField : ObservableField<Int>()
class FloatObservableField   : ObservableField<Float>()

typealias  EventHandler      = (()->Unit)?
typealias  ErrorEventHandler = ((error: Throwable)->Unit)?
typealias  DataEventHandler  = ((direction: SpeedTestDirection, percent: Float, bitsPerSec: BigDecimal)->Unit)?

enum class SpeedTestDirection   { IDLE, DOWNLOAD, UPLOAD }
enum class NetworkType { WIFI, CELLULAR, UNKNOWN }
data class SpeedTestResult(var direction: SpeedTestDirection, var percent: Float, var transferRate: BigDecimal)

@Target(AnnotationTarget.FIELD)
annotation class LookupDataBinding(val name: String)
