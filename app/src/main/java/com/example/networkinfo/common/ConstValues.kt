package com.example.networkinfo.common

object Const
{
    const val LOG_TAG                  = "O2_PING"
    const val SPEED_TEST_SERVER_URI_DL = "ftp://speedtest.tele2.net/10MB.zip"
    const val SPEED_TEST_SERVER_URI_UL = "ftp://speedtest.tele2.net/upload/"
    const val UPLOAD_FILE_SIZE         = 10000000
    const val EMPTY_STRING             = ""
    const val IPAPI_URL                = "https://api.ipify.org"
    const val LOOKUP_URL               = "http://ip-api.com/json/"
    const val SKIP_PACKETS_COUNT       = 0
}