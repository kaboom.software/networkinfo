package com.example.networkinfo.common


import android.util.Log

@Target(AnnotationTarget.FIELD)
annotation class ResetMe

fun resetMarkedFields(obj: Any)
{
    try {
        obj.javaClass.declaredFields
            .filter { it.isAnnotationPresent(ResetMe::class.java) }
            .map{ it.get(obj) }
            .forEach {
                when(it){
                    is StringObservableField    -> it.set(Const.EMPTY_STRING)
                    is IntegerObservableField   -> it.set(0)
                    is FloatObservableField     -> it.set(0f)
                    is FloatObservableCalcField -> it.reset()
                    else -> Log.e(Const.LOG_TAG, "Reset by annotation not implemented yet for type ${it.javaClass}")
                }
            }

    } catch (e: Exception) {
        Log.e(Const.LOG_TAG, "resetView error: ${e.localizedMessage}")
    }
}